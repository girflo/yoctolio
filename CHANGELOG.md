# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2023-04-14
### Added
- Lazy loading for youtube videos
- Lazy loading for vimeo videos

### Changed
- Declutter youtube videos
- Declutter vimeo videos
- Ask for a project name when using init

## [0.3.0] - 2022-02-05
### Added
- Add init flag
- Support for description
- Support for multiple tags
- help and version flags

### Changed
- To build the website you now need to use the build flag

## [0.2.0] - 2021-12-16
### Added
- Support for vimeo 
- Add id css selector in tag titles 
