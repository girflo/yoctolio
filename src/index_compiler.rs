use crate::html_compiler;
use crate::sanitizer;
use crate::settings_serializer;
use crate::template_filler;
use crate::template_finder;
use crate::video_serializer;

pub fn call() {
    let video_section = get_all_category_sections();
    let mut fillings = Vec::new();
    fillings.push(("{{video_section}}", video_section.as_str()));
    let template_index = template_finder::call("index.html");
    let index_page_content = template_filler::call(&template_index, fillings);
    html_compiler::call(index_page_content, "public/index.html");
}

fn get_all_category_sections() -> String {
    let sections = settings_serializer::call("config/config.txt");
    let mut section_content: String = "".to_string();
    for section_data in sections {
        let config_file = get_config_file_path(&section_data.value);
        let videos = get_videos_section_html(&config_file);
        let mut fillings = Vec::new();
        let title = section_data.key.as_str();
        let id = sanitizer::call(&title);
        fillings.push(("{{title}}", title));
        fillings.push(("{{id}}", id.as_str()));
        fillings.push(("{{videos}}", videos.as_str()));
        let section = template_filler::call("templates/category_section.html", fillings);
        section_content.push_str(&section);
    }
    section_content
}

fn get_config_file_path(config_filename: &str) -> String {
    let mut config_path: String = "config/".to_owned();
    config_path.push_str(config_filename);
    config_path
}

fn get_videos_section_html(file: &str) -> String {
    let mut html_content: String = "".to_owned();
    let video_serializer = video_serializer::Videos::new(file);
    for year in video_serializer.ordered_years() {
        let mut year_fillings = Vec::new();
        let mut yearly_videos: String = "".to_owned();
        for video in video_serializer.select_by_year(year).videos {
            let mut fillings = Vec::new();
            let tags = get_tag_section_html(video.tag_id);
            let description_html = get_description_section_html(video.description.as_str());
            fillings.push(("{{title}}", video.title.as_str()));
            fillings.push(("{{video_id}}", video.video_id.as_str()));
            fillings.push(("{{description}}", &description_html));
            fillings.push(("{{tags}}", tags.as_str()));
            let video_html = template_filler::call(&video.template_path, fillings);
            yearly_videos.push_str(&video_html)
        }
        let year_string = year.to_string();
        year_fillings.push(("{{year}}", year_string.as_str()));
        year_fillings.push(("{{yearly_videos}}", yearly_videos.as_str()));
        let year_html = template_filler::call("templates/year_section.html", year_fillings);
        html_content.push_str(&year_html);
    }
    return html_content;
}

fn get_description_section_html(description: &str) -> String {
    if description == "" {
        "".to_string()
    } else {
        let description_filling = vec![("{{description}}", description)];
       template_filler::call("templates/description.html", description_filling)
    }
}

fn get_tag_section_html(tags: String) -> String {
    let all_tags = settings_serializer::call("config/tags.txt");
    let mut html_section: String = "".to_owned();
    let tags_ids: Vec<String> = tags.split("-").map(str::to_string).collect();
    for tag_id in tags_ids {
        let mut tag_fillings = Vec::new();
        let tag = find_tag_name(&all_tags, tag_id);
        tag_fillings.push(("{{tag}}", tag.as_str()));
        let single_tag_html = template_filler::call("templates/tag.html", tag_fillings);
        html_section.push_str(&single_tag_html);
    }
    return html_section;
}

fn find_tag_name(all_tags: &Vec<settings_serializer::Settings>, tag_id: String) -> String {
    let matching_tag = all_tags.iter().find( |s| s.key == tag_id);
    match matching_tag {
        Some(c) => return c.value.to_string(),
        None => return "".to_string(),
    }
}
