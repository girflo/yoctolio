pub fn call(template_name: &str) -> String {
    format!("templates/{}", template_name)
}
