use regex::Regex;

pub fn call(dirty_string: &str) -> String {
    let re = Regex::new(r"\W").unwrap();
    let step_one = re.replace_all(&dirty_string, '-'.to_string()).to_string();
    let re_two = Regex::new(r"-{2,}").unwrap();
    let step_two = re_two.replace_all(&step_one, '-'.to_string()).to_string();
    step_two.to_lowercase().to_string()
}
