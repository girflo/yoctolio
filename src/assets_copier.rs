use crate::folder_copier;
use std::path::Path;

pub fn call() {
    if Path::new("templates/assets/").is_dir() {
        let copier = folder_copier::call("templates/assets/", "public/assets/");

        let _copier = match copier{
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        };
    }
}
