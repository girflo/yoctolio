use crate::data_extractor;

pub fn call(file: &str) -> Vec<Settings> {
    let structured_data = data_extractor::call(file);
    let mut settings = Vec::new();
    for data in structured_data {
        let setting = Settings::new(data);
        settings.push(setting);
    }
    settings
}

pub struct Settings {
    pub key: String,
    pub value: String
}

impl Settings {
    fn new(structured_data: data_extractor::Data) -> Settings {
        let key = structured_data.data.first().expect("Key is missing");
        let value = structured_data.data.last().expect("Value is missing");

        Settings {
            key: key.to_string(),
            value: value.to_string(),
        }

    }
}
