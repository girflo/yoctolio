use crate::template_initializer;

pub fn call(template_path: &str, fillings: Vec<(&str, &str)>) -> String {
    let mut template = template_initializer::call(template_path);

    for filling in fillings {
        template = str::replace(&template, filling.0, filling.1);
    }
    template
}
