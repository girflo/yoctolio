use std::fs;
use crate::html_compiler;
use crate::template_initializer;

pub fn call() {
    let page_path = "templates/pages/";
    let pages = fs::read_dir(&page_path).unwrap();

    for page in pages {
        let path_as_string = page.unwrap().path().display().to_string();
        let page_content = template_initializer::call(&path_as_string);
        let output_filename = str::replace(&path_as_string, &page_path, "public/");
        html_compiler::call(page_content, &output_filename);
    }
}
