use regex::Regex;
use crate::file_content_loader;

pub fn call(template_path: &str) -> String {
    let template = file_content_loader::call(template_path);
    let initialized_template = replace_partials(&template);
    initialized_template
}

fn replace_partials(template_content: &str) -> String {
    let re = Regex::new(r"(?P<p>\{{2}!{1}\w+\}{2})").unwrap();
    let mut template_with_partials = template_content.to_string();
    for cap in re.captures_iter(template_content) {
        let partial_content = get_partial_content(&cap[1]);
        template_with_partials = str::replace(&template_with_partials, &cap[1], &partial_content )
    }
    template_with_partials
}

fn get_partial_path(partial_match: &str) -> String {
    let change_head = str::replace(partial_match, "{{!", "templates/partials/");
    str::replace(&change_head, "}}", ".html")
}

fn get_partial_content(partial_match: &str) -> String {
    let partial_path = get_partial_path(partial_match);
    file_content_loader::call(&partial_path)
}
