use std::io::{BufRead, BufReader};
use std::fs::File;

pub fn call(file: &str) -> Vec<Data> {
    let reader = BufReader::new(File::open(file).expect("Cannot open file"));
    let mut data_vector = Vec::new();
    for line in reader.lines() {
        let data = build_data(line.unwrap());
        data_vector.push(data);
    }
    return data_vector;
}

fn build_data(file_line: String) -> Data {
    let raw_data: Vec<String> = file_line.split(", ").map(str::to_string).collect();
    let structured_data = Data {
        data: raw_data,
    };
    structured_data
}

pub struct Data {
    pub data: Vec<String>
}
