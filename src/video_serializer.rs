use crate::data_extractor;

#[derive(Clone)]
pub struct Video {
    pub title: String,
    pub video_id: String,
    pub tag_id: String,
    pub year: i32,
    pub description: String,
    pub template_path: String
}

impl Video {
    fn new(structured_data: data_extractor::Data) -> Video {
        let key = &structured_data.data[0];
        let value = &structured_data.data[1];
        let tag_id = &structured_data.data[2];
        let year: i32 = structured_data.data[3].parse().unwrap();
        let description = structured_data.data.get(4);
        let empty_description = "".to_string();
        let mut video_id = "".to_string();
        let mut template = "".to_string();
        if value.contains("youtube.com") {
            video_id.push_str(&find_youtube_id(value));
            template.push_str("templates/youtube_video.html");
        } else {
            video_id.push_str(&find_vimeo_id(value));
            template.push_str("templates/vimeo_video.html");
        }

        Video {
            title: key.to_string(),
            video_id: video_id.to_string(),
            tag_id: tag_id.to_string(),
            year: year,
            description: description.unwrap_or(&empty_description).to_string(),
            template_path: template
        }
    }
}

fn find_youtube_id(youtube_url: &str) -> String {
    return str::replace(youtube_url, "https://www.youtube.com/watch?v=", "");
}

fn find_vimeo_id(vimeo_url: &str) -> String {
    return str::replace(vimeo_url, "https://vimeo.com/", "");
}


pub struct Videos {
    pub videos: Vec<Video>
}

impl Videos {
    pub fn new(file: &str) -> Videos {
        let structured_data = data_extractor::call(file);
        let mut video_templates = Vec::new();
        for data in structured_data {
            let video = Video::new(data);
            video_templates.push(video);
        }
        let all_videos = Videos {
            videos: video_templates
        };
        all_videos
    }

    pub fn select_by_year(&self, year: i32) -> Videos {
        let mut cloned_videos_list = self.videos.clone();
        cloned_videos_list.retain( |v| v.year == year);
        Videos {
            videos: cloned_videos_list.to_vec()
        }
    }

    pub fn ordered_years(&self) -> Vec<i32> {
        let mut all_years = Vec::new();
        for v in &self.videos {
            all_years.push(v.year);
        }
        all_years.clear_duplicates();
        all_years
    }
}

trait Dedup<T: PartialEq + Clone> {
    fn clear_duplicates(&mut self);
}

impl<T: PartialEq + Clone> Dedup<T> for Vec<T> {
    fn clear_duplicates(&mut self) {
        let mut already_seen = vec![];
        self.retain(|item| match already_seen.contains(item) {
            true => false,
            _ => {
                already_seen.push(item.clone());
                true
            }
        })
    }
}
