use std::fs;

pub fn call(project_name: &String) -> Result<(), std::io::Error> {
    match create_directories(project_name) {
        Err(e) => return Err(e),
        _ => copy_all_files(project_name),
    }
}

fn create_directories(project_name: &String) -> Result<(), std::io::Error> {
    let directories = vec![
        project_name.clone(),
        format!("{}/config", project_name),
        format!("{}/templates", project_name),
        format!("{}/templates/assets", project_name),
        format!("{}/templates/assets/css", project_name),
        format!("{}/templates/pages", project_name),
        format!("{}/templates/partials", project_name),
    ];
    for directory in directories {
        println!("create: {:?}", &directory);
        let directory_copy = fs::create_dir_all(directory);
        match directory_copy {
            Err(e) => return Err(e),
            _ => (),
        }
    }
    Ok(())
}

fn copy_all_files(project_name: &String) -> Result<(), std::io::Error> {
    println!("create: config files");
    let config = include_str!("init/config/config.txt");
    fs::write(format!("{}/config/config.txt", project_name), config)?;
    let clips = include_str!("init/config/clips.txt");
    fs::write(format!("{}/config/clips.txt", project_name), clips)?;
    let tags = include_str!("init/config/tags.txt");
    fs::write(format!("{}/config/tags.txt", project_name), tags)?;
    println!("create: template files");
    let piddly = include_str!("init/templates/assets/css/piddly.css");
    fs::write(
        format!("{}/templates/assets/css/piddly.css", project_name),
        piddly,
    )?;
    let style = include_str!("init/templates/assets/css/style.css");
    fs::write(
        format!("{}/templates/assets/css/style.css", project_name),
        style,
    )?;
    let category_section = include_str!("init/templates/category_section.html");
    fs::write(
        format!("{}/templates/category_section.html", project_name),
        category_section,
    )?;
    let description = include_str!("init/templates/description.html");
    fs::write(
        format!("{}/templates/description.html", project_name),
        description,
    )?;
    let index = include_str!("init/templates/index.html");
    fs::write(format!("{}/templates/index.html", project_name), index)?;
    let menu_item = include_str!("init/templates/menu_item.html");
    fs::write(
        format!("{}/templates/menu_item.html", project_name),
        menu_item,
    )?;
    let menu = include_str!("init/templates/menu.html");
    fs::write(format!("{}/templates/menu.html", project_name), menu)?;
    let tag = include_str!("init/templates/tag.html");
    fs::write(format!("{}/templates/tag.html", project_name), tag)?;
    let vimeo_video = include_str!("init/templates/vimeo_video.html");
    fs::write(
        format!("{}/templates/vimeo_video.html", project_name),
        vimeo_video,
    )?;
    let year_section = include_str!("init/templates/year_section.html");
    fs::write(
        format!("{}/templates/year_section.html", project_name),
        year_section,
    )?;
    let youtube_video = include_str!("init/templates/youtube_video.html");
    fs::write(
        format!("{}/templates/youtube_video.html", project_name),
        youtube_video,
    )?;
    let about = include_str!("init/templates/pages/about.html");
    fs::write(
        format!("{}/templates/pages/about.html", project_name),
        about,
    )?;
    let head = include_str!("init/templates/partials/head.html");
    fs::write(
        format!("{}/templates/partials/head.html", project_name),
        head,
    )?;
    let header = include_str!("init/templates/partials/header.html");
    fs::write(
        format!("{}/templates/partials/header.html", project_name),
        header,
    )?;
    let navigation = include_str!("init/templates/partials/navigation.html");
    fs::write(
        format!("{}/templates/partials/navigation.html", project_name),
        navigation,
    )?;
    Ok(())
}
