use std::io::prelude::*;
use std::fs::File;

pub fn call(template_name: &str) -> String {
    let mut file = File::open(template_name).expect("Unable to open the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Unable to read the file");
    return contents
}
