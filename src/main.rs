// use clap::{App, AppSettings};
use clap::{arg, Command};
mod assets_copier;
mod data_extractor;
mod file_content_loader;
mod folder_copier;
mod html_compiler;
mod index_compiler;
mod initializer;
mod pages_compiler;
mod sanitizer;
mod settings_serializer;
mod template_filler;
mod template_finder;
mod template_initializer;
mod video_serializer;

fn main() {
    const VERSION: &str = env!("CARGO_PKG_VERSION");
    let matches = Command::new("Yoctolio")
        .about("Static video portfolio generator")
        .version(VERSION)
        .subcommand_required(true)
        .arg_required_else_help(true)
        .author("Flo Girardo <3042155-girflo@users.noreply.gitlab.com>")
        .subcommand(
            Command::new("init")
                .about("Initialize a portfolio")
                .arg(arg!(<NAME> "Name of the portfolio"))
                .arg_required_else_help(true),
        )
        .subcommand(
            Command::new("build").about("Compile an existing portfolio into a usable website"),
        )
        .get_matches();

    match matches.subcommand() {
        Some(("init", sub_matches)) => {
            match initializer::call(sub_matches.get_one::<String>("NAME").expect("required")) {
                Err(_e) => {
                    println!("An error occured when creating folder, the initilization was stopped")
                }
                _ => (),
            }
        }
        Some(("build", _sub_matches)) => {
            assets_copier::call();
            index_compiler::call();
            pages_compiler::call();
        }
        _ => unreachable!(),
    }
}
