use std::io::Write;

pub fn call(page_content: String, output_file_path: &str) {
    if let Err(e) = write_html_page(page_content, output_file_path) {
        println!("Writing error: {}", e.to_string());
    }
}

fn write_html_page(page_content: String, output_file_path: &str) -> std::io::Result<()>  {
    let mut f = std::fs::OpenOptions::new().write(true).create(true).truncate(true).open(output_file_path)?;
    f.write_all(page_content.as_bytes())?;
    f.flush()?;
    Ok(())
}
